// README.txt file 
// By Abdullaziz alhumoudi    
// 10/9/2014   ..   CS469

1.    List the common reasons for making a branch in the repository.
      there are many reasons why a branch is created. 
      one reason is the application is going in another direction, 
      or the developer is trying something but not sure if its gonna work so he doesnt add it to the main branch, 
      or multiple devoloper are working on a project on thier speceifc part and then merge thier work together when thier done that part
	
2.    For each of these reasons, briefly discuss whether you would normally expect such a branch to cause a conflict when merged and why.
      first reason: yes it might cause a conflict because trying something different from the original while still updating the original 
	would create different project branches which would most lkily produce a conflcit 
      second reason: similer to the first reason, depends on the progress on the main branch and which updates were made. if a new file is made it wont confict
	but if a file has been adjusted differently than the main branch then it conflicts
      third: same as above

3.    Describe what happened when you attempted to merge branch B into master above. 
      it told me i have a conflict so i had to resolve the conflict to proceed. i resloved by openning the file.

4.    Describe what you did to resolve the problem and complete the merge.
      opened the the file, deleteing the extra files added by the sourceTree and appending the lined together. 
	then i clicked on the option to resolve the conflict. and wrote a commit comment then commited

5.    Describe what you had to do in the last two steps to successfully merge O�s changes into F and push from F to the remote.
	I had to pull the remote repo from bitbucket and merge it with my F repo. after doing that it found a conflict so i simply resloved that conflict 
	and commited it with a new messege. the pushed it to bitbucket